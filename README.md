# Getting Started

With the latest Node LTS installed, run the following commands:

''  

npm install  

cd client  

npm install
  
''


To start the Node/React servers, from the project root just run:

''  

npm start  

''

You should now have:

A Node server running on port 3001,
A create-react-app server running on port 3000


# Top Level Design

Layout for App component is based on.
 
	Section Element 

		Card Display Component 

			Title / Examples / Explanation  

			Search Bar

			Paginated Cards Component 
			
				HTML elements rendered from Card instances.
			
				Pagination Controls
			
			End of Paginated Cards Component 

		End of Card Display

	End of Section Element


# Overview of Tasks

Search bar for the user to input a card name to search. 
Using the input string, make a call to your Node endpoint (Scryfall API) to lookup cards.  

https://scryfall.com/docs/api/cards/search


The search should work without the user needing to click a button.
And it should be impossible for a user to submit more than 1 API request per second using this search bar.

Display the card results to the user in the most user friendly way you can come up with.  Feel free to be creative.
All cards must display the following: The card's image(s), name, set name, number, and rarity.


Interface is styled using Tailwindcss.  

https://tailwindcss.com/docs/guides/create-react-app



# Breakdown of Tasks Completed


Finished version of search where results are displayed as multiple pages.
Currently the amount of cards per page is set at 8 but this is configurable.
Each row of results is split into fours cards per row.

Searching of the ScryFall API is attempted whenever the onBlur event is triggered on the search bar element.
If a search term is not present or the search term is the same as the previous, searching is not performed. 

URI is now loaded for env file as REACT_APP_SEARCH.

# Next Tasks

Custom message needs to be shown and the page remains unaffected when a search produces no results. 

CSS Formating for example searches - Would like text aligned left within a div that is centered.

Packaging for production.



