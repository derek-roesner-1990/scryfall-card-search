import React from 'react';
import ReactPaginate from 'react-paginate';
import { useState } from 'react';
import { v4 as uuid } from 'uuid';

const searchBaseUri : string = (process.env.REACT_APP_SEARCH);
let searchQuery : string = "";

/* Card styling is based on - https://tailgrids.com/components/cards
   Searchbar styling based on  - https://tailwindcss.com  */ 
export function CardDisplay(){
		
	// Allows for tracking of the presentation rendered for each Cards instance. 
	// This is done via monitoring of calls to setCards().
	const [cards, setCards] = useState([]);
	
	const cardsPerPage : number = 8;
	const [cardOffset, setCardOffset] = useState(0);
	const pageCount : number = Math.ceil(cards.length / cardsPerPage);
	const endOffset : number = cardOffset + cardsPerPage;
	const currentCards : number = cards.slice(cardOffset, endOffset);
	
	function handleSearch(this:HTMLInputElement) {

		let currentQuery : String = document.querySelector("#search_bar").value;
			
		if(currentQuery === "" || currentQuery === searchQuery){
			return;
		}
				
		if(currentQuery !== searchQuery){
			searchQuery = currentQuery;
		}
		
		let timeout = setTimeout( () => { 
	
			let searchFull = searchBaseUri + searchQuery;
				
			fetchCards(searchFull)
			.then( (cards) => {
				if(cards !== undefined){
					setCards(cards);
				}
			}).catch( (error) => {
				console.log(error)
			});
		}, 1500);

	}

	// Invoke when user click to request another page of results.
	function handlePageClick(event) {
		let newOffset = (event.selected * cardsPerPage) % cards.length;
		setCardOffset(newOffset);
	}

	function fetchCards(resourceUrl: string): Promise<T> {
		
			let cards = [];
			
			console.log("Attempting to Fetch Info")
			
			return fetch(resourceUrl)
		
		.then(response => {
			return response.json();
		}).then(json => {

			
			if(json.code === 'not_found'){
				return undefined
			}
			
			json.data.forEach (function(value) {			
				
				var cardDict = {
					graphic : "Unknown",
					name : "Unknown",
					setName : "Unknown",
					number : "Unknown",
					rarity : "Unknown",
				}
				
				if(value.image_uris !== null && value.image_uris !== undefined){
					cardDict.graphic = value.image_uris.png;
				}
				
				if(value.name !== null && value.name !== undefined){
					cardDict.name = value.name
				}
				
				if(value.set_name !== null && value.set_name !== undefined){
					cardDict.setName = value.set_name
				}
				
				if(value.collector_number !== null && value.collector_number !== undefined){
					cardDict.number = value.collector_number
				}
				
				if(value.rarity !== null && value.rarity !== undefined){
					cardDict.rarity = value.rarity
				}
			
				cards.push(cardDict);
			});
		
			return cards;
		}).catch(error => {
			console.log(error);
		});
	}

	class Card extends React.Component {

		constructor(props){
			super(props)
		}
		
		render() {
					return(
						<div className="w-1/4 px-5 m-overflow-hidden rounded-m bg-slate-800 outline-black" key={this.props.id}>
						<img id ="card_image" src={this.props.graphic} alt="Card" className="w-full" />
							<div className="p-8 text-center">
								<p id="card_name" className="text-2xl mb-4 block font-semibold ">
									{this.props.name}
								</p>
								<p id="set_name" className=" text-l mb-4 block font-semibold">
									<u>Associated Card Set</u> <br/>{this.props.setName}
								</p>
								<p id="collectors_number" className="text-l mb-4 block font-semibold">
									 <u>Collectors Number</u> <br/> {this.props.number}
								</p>
								<p id="rarity" className="text-l mb-4 block font-semibold">
									 <u> Rarity </u> <br/> {this.props.rarity}
								</p>
							 </div>
						</div>		
					);	
		}	
	}

	// The amount of cards displayer per row is determined by 'w=1/4' within the className of the outermost div.
	// Please see the https://tailwindcss.com/docs/width for more info.
	class PaginatedCards extends React.Component {
				
		render() {
			
			var cardsHTML = currentCards.map(cardDict => 
				<Card id={uuid()} graphic={cardDict.graphic} name={cardDict.name} setName={cardDict.setName} number={cardDict.number} rarity={cardDict.rarity}/>
			)
			
			return (
				<div className="py-2">
					<div className="flex flex-wrap"> {cardsHTML} </div>
					<div>
						<ReactPaginate
						className="flex flex-wrap flex-row justify-evenly py-5 bg-slate-800 font-bold font-6xl"
						previousLinkClassName="page-link"
						pageLinkClassName="page-link"
						nextLinkClassName="page-link"
						breakLabel="..."
						nextLabel="next >"
						onPageChange={handlePageClick}
						pageRangeDisplayed={5}
						pageCount={pageCount}
						previousLabel="< previous"
						renderOnZeroPageCount={null}
						/>
					</div>
				</div>
			);
		}
	}
	
	return (
		<div className="container mx-auto space-y-1 text-center">
			<p className="text-4xl pb-5"> <b> Scryfall Card Search </b> </p>
			<div className="pb-5 w-1/2 mx-auto">
				<p className="pb-1"> <u> Example Searches </u> </p>
				<p> <b> Fire </b>- Search for card names containing the pattern "Fire".</p>
				<p> <b> !Fire or !"Ancestor's Chosen" </b>- Search for card names "Fire" or "Ancestor's Chosen".</p>
				<p> <b> c:green </b> - Search for all cards with color of Green.</p>
				<p> <b> r:mythic </b> - Search for all cards with the rarity of Mythic.</p>
				<p> <b> cn:1 </b> - Search for all cards with the collectors number of 1.</p>
			</div>
			<div> <a href="https://scryfall.com/docs/syntax">  <u> Syntax Reference - https://scryfall.com/docs/syntax </u>  </a> </div>
			<div title="Click Page To Begin Search" className="text-4xl py-3 font-bold"> Search Term  </div>
			<input id="search_bar" title="Click Page To Begin Search"  onBlur={handleSearch} className="focus:ring-2 focus:ring-blue-500 focus:outline-none appearance-none w-full text-m py-2 pl-3 leading-6 text-slate-900 placeholder-slate-400 rounded-md ring-1 ring-slate-200 shadow-sm" type="text" aria-label="Filter projects" placeholder="Search By Card..."></input>		
			<PaginatedCards />
		</div>
	);

	
}