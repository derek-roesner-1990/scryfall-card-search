import './App.css';
import { CardDisplay } from './lib/cardDisplay.js';

function App() {	
  return (
	<section className="py-10">
			<CardDisplay />
	</section>
  );
}

export default App;    